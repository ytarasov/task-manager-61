package ru.t1.ytarasov.tm.service;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.ytarasov.tm.api.service.IReceiverService;

import javax.jms.*;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class ReceiverService implements IReceiverService {

    @NotNull
    private static final String QUEUE = "LOGGER";

    @NotNull
    @Autowired
    private ConnectionFactory connectionFactory;

    @Override
    @SneakyThrows
    public void receive(@NotNull MessageListener listener) {
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();

        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Queue destination = session.createQueue(QUEUE);
        @NotNull final MessageConsumer messageConsumer = session.createConsumer(destination);
        messageConsumer.setMessageListener(listener);
    }
}
