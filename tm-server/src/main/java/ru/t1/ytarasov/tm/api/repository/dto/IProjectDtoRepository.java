package ru.t1.ytarasov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ytarasov.tm.dto.model.ProjectDTO;

import java.util.List;

@Repository
public interface IProjectDtoRepository extends IUserOwnedDtoRepository<ProjectDTO> {

    @Nullable
    @Query("SELECT p FROM ProjectDTO p WHERE p.userId = :userId ORDER BY :sortType")
    List<ProjectDTO> findAllWithSort(@NotNull @Param("sortType") final String sortType);

    @Nullable
    @Query("SELECT p FROM ProjectDTO p WHERE p.userId = :userId ORDER BY :sortType")
    List<ProjectDTO> findAllWithUserIdAndSort(
            @NotNull @Param("userId") final String userId,
            @NotNull @Param("sortType") final String sortType
    );

    @Nullable
    @Query("SELECT p FROM ProjectDTO p WHERE p.userId = :userId")
    List<ProjectDTO> findAllByUserId(@NotNull @Param("userId") final String userId);

    @Nullable
    @Query("SELECT p FROM ProjectDTO p WHERE p.id = :id AND p.userId = :userId")
    ProjectDTO findOneById(@NotNull @Param("userId") final String userId, @NotNull @Param("id") final String id);

    @Query("SELECT COUNT(p) FROM ProjectDTO p WHERE p.userId = :userId")
    Long getSize(@NotNull @Param("userId") final String id);

    @Query("SELECT COUNT(1) = 1 FROM ProjectDTO p WHERE p.id = :id AND p.userId = :userId")
    Boolean existsByIdAndUserId(@NotNull @Param("userId") final String userId, @NotNull @Param("id") final String id);


}
