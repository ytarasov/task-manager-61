package ru.t1.ytarasov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.ytarasov.tm.api.repository.model.ITaskRepository;
import ru.t1.ytarasov.tm.api.repository.model.IUserRepository;
import ru.t1.ytarasov.tm.api.service.model.ITaskService;
import ru.t1.ytarasov.tm.enumerated.Sort;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.exception.entity.TaskNotFoundException;
import ru.t1.ytarasov.tm.exception.field.*;
import ru.t1.ytarasov.tm.exception.user.UserNotFoundException;
import ru.t1.ytarasov.tm.model.Task;
import ru.t1.ytarasov.tm.model.User;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

@Service
public final class TaskService
        extends AbstractUserOwnedService<Task> implements ITaskService {

    @NotNull
    @Autowired
    private ITaskRepository repository;

    @NotNull
    @Autowired
    private IUserRepository userRepository;

    @Nullable
    @Override
    public List<Task> findAll() throws Exception {
        return repository.findAll();
    }

    @Override
    public Long getSize() throws Exception {
        return repository.count();
    }

    @NotNull
    @Override
    public Task add(@Nullable Task model) throws Exception {
        if (model == null) throw new TaskNotFoundException();
        repository.save(model);
        return model;
    }

    @Nullable
    @Override
    public Collection<Task> add(@Nullable Collection<Task> models) throws Exception {
        if (models == null || models.isEmpty()) throw new TaskNotFoundException();
        repository.saveAll(models);
        return models;
    }

    @Override
    public boolean existsById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsById(id);
    }

    @Nullable
    @Override
    public Task findOneById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findById(id).orElse(null);
    }

    @Override
    public void clear() throws Exception {
        repository.deleteAll();
    }

    @Nullable
    @Override
    public Task remove(@Nullable Task model) throws Exception {
        if (model == null) throw new TaskNotFoundException();
        repository.delete(model);
        return model;
    }

    @Nullable
    @Override
    public Task removeById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        repository.deleteById(id);
        return task;
    }

    @NotNull
    @Override
    public Task update(@Nullable Task model) throws Exception {
        if (model == null) throw new TaskNotFoundException();
        model.setUpdated(new Date());
        repository.save(model);
        return model;
    }

    @Nullable
    @Override
    public List<Task> findAll(@Nullable Sort sort) throws Exception {
        if (sort == null) return findAll();
        return repository.findAllWithSort(getSortType(sort));
    }

    @Nullable
    @Override
    public List<Task> findAll(@Nullable String userId, @Nullable Sort sort) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll();
        return repository.findAllWithUserIdAndSort(userId, getSortType(sort));
    }

    @Nullable
    @Override
    public List<Task> findByProjectId(@Nullable String userId, @Nullable String projectId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        return repository.findByProjectId(userId, projectId);
    }

    @NotNull
    @Override
    public Task create(@Nullable String userId, @Nullable String name, @Nullable String description) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        return add(userId, task);
    }

    @NotNull
    @Override
    public Task updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return update(task);
    }

    @NotNull
    @Override
    public Task changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        task.setUpdated(new Date());
        repository.save(task);
        return task;
    }

    @Override
    public void clear(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final List<Task> tasks = findAll(userId);
        if (tasks == null || tasks.isEmpty()) throw new TaskNotFoundException();
        repository.deleteAll(tasks);
    }

    @Override
    public boolean existsById(@Nullable String userId, @Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.existsById(id);
    }

    @Nullable
    @Override
    public List<Task> findAll(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAllWithUserId(userId);
    }

    @Nullable
    @Override
    public Task findOneById(@Nullable String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(userId, id);
    }

    @Override
    public Long getSize(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.getSize(userId);
    }

    @Nullable
    @Override
    public Task removeById(@Nullable String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        repository.delete(task);
        return task;
    }

    @NotNull
    @Override
    public Task add(@Nullable String userId, @Nullable Task task) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (task == null) throw new TaskNotFoundException();
        @Nullable final User user = userRepository.findById(userId).orElse(null);
        if (user == null) throw new UserNotFoundException();
        task.setUser(user);
        return add(task);
    }

    @Nullable
    @Override
    public Task remove(@Nullable String userId, @Nullable Task model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new TaskNotFoundException();
        @Nullable final Task task = findOneById(userId, model.getId());
        if (task == null) throw new TaskNotFoundException();
        return remove(task);
    }

}
