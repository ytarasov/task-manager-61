package ru.t1.ytarasov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.t1.ytarasov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @Nullable
    @Query("SELECT t FROM Task t ORDER BY :sortOrder")
    List<Task> findAllWithSort(@NotNull @Param("sortOrder") final String sortOrder);

    @Nullable
    @Query("SELECT t FROM Task t WHERE t.user.id = :userId ORDER BY :sortOrder")
    List<Task> findAllWithUserIdAndSort(
            @NotNull @Param("userId") final String userId,
            @NotNull @Param("sortOrder") final String sortOrder
    );

    @Nullable
    @Query("SELECT t FROM Task t WHERE t.user.id = :userId")
    List<Task> findAllWithUserId(@NotNull @Param("userId") final String userId);

    @Nullable
    @Query("SELECT t FROM Task t WHERE t.user.id = :userId AND t.id = :id")
    Task findOneById(@NotNull @Param("userId") final String userId, @NotNull @Param("id") final String id);

    @Query("SELECT COUNT(t) FROM Task t WHERE t.user.id = :userId")
    Long getSize(@NotNull @Param("userId") final String userId);

    @Query("SELECT COUNT(1) = 1 FROM Task t WHERE t.id = :id AND t.user.id = :userId")
    Boolean existsByIdWithUserId(@NotNull @Param("userId") final String userId, @NotNull @Param("id") final String id);

    @Nullable
    @Query("SELECT t FROM Task t WHERE t.user.id = :userId AND t.project.id = :projectId")
    List<Task> findByProjectId(
            @NotNull @Param("userId") final String userId,
            @NotNull @Param("projectId") final String projectId
    );

}
