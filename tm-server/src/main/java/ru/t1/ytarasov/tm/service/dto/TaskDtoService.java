package ru.t1.ytarasov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.ytarasov.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.ytarasov.tm.api.service.dto.ITaskDtoService;
import ru.t1.ytarasov.tm.enumerated.Sort;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.exception.entity.TaskNotFoundException;
import ru.t1.ytarasov.tm.exception.field.*;
import ru.t1.ytarasov.tm.dto.model.TaskDTO;

import java.util.*;

@Service
public final class TaskDtoService extends AbstractUserOwnedDtoService<TaskDTO> implements ITaskDtoService {

    @NotNull
    @Autowired
    private ITaskDtoRepository repository;

    @NotNull
    @Override
    public TaskDTO add(@Nullable TaskDTO task) throws Exception {
        if (task == null) throw new TaskNotFoundException();
        repository.saveAndFlush(task);
        return task;
    }

    @NotNull
    @Override
    public Collection<TaskDTO> add(@Nullable Collection<TaskDTO> models) throws Exception {
        if (models == null || models.isEmpty()) throw new TaskNotFoundException();
        repository.saveAll(models);
        return models;
    }

    @NotNull
    @Override
    public TaskDTO create(@Nullable final String userId,
                          @Nullable final String name,
                          @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final TaskDTO task = new TaskDTO(name, description);
        task.setUserId(userId);
        return add(task);
    }

    @Nullable
    @Override
    public List<TaskDTO> findAll() {
        return repository.findAll();
    }

    @Nullable
    @Override
    public List<TaskDTO> findAll(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAllByUserId(userId);
    }

    @Nullable
    @Override
    public List<TaskDTO> findAll(@Nullable Sort sort) throws Exception {
        if (sort == null) return findAll();
        else return repository.findAllWithSort(getSortType(sort));
    }

    @Nullable
    @Override
    public List<TaskDTO> findAll(@Nullable String userId, @Nullable Sort sort) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        else return repository.findAllWithUserIdAndSort(userId, getSortType(sort));
    }

    @Nullable
    @Override
    public TaskDTO findOneById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findById(id).orElse(null);
    }

    @Nullable
    @Override
    public TaskDTO findOneById(@Nullable String userId, @Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findOneById(userId, id);
    }

    @Override
    public Long getSize() throws Exception {
        return repository.count();
    }

    @Override
    public Long getSize(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.getSize(userId);
    }

    @NotNull
    @Override
    public TaskDTO remove(@Nullable TaskDTO task) throws Exception {
        if (task == null) throw new TaskNotFoundException();
        repository.delete(task);
        return task;
    }

    @Override
    public void removeById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final TaskDTO task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        repository.deleteById(id);
    }

    @Nullable
    @Override
    public TaskDTO removeById(@Nullable String userId, @Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        return remove(task);
    }

    @Nullable
    @Override
    public TaskDTO add(@Nullable String userId, @Nullable TaskDTO model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new TaskNotFoundException();
        return add(model);
    }

    @Nullable
    @Override
    public TaskDTO remove(@Nullable String userId, @Nullable TaskDTO model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new TaskNotFoundException();
        @Nullable final TaskDTO task = findOneById(userId, model.getId());
        if (task == null) throw new TaskNotFoundException();
        return remove(task);
    }

    @Override
    public Boolean existsById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsById(id);
    }

    @Override
    public Boolean existsById(@Nullable String userId, @Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.existsByIdAndUserId(userId, id);
    }

    @Override
    public @NotNull List<TaskDTO> findAllTasksByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        return repository.findAllTasksByProjectId(userId, projectId);
    }

    @NotNull
    @Override
    public TaskDTO update(@NotNull final TaskDTO task) throws Exception {
        task.setUpdated(new Date());
        repository.saveAndFlush(task);
        return task;
    }

    @NotNull
    @Override
    public TaskDTO updateById(@Nullable final String userId,
                              @Nullable final String id,
                              @Nullable final String name,
                              @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return update(task);
    }

    @NotNull
    @Override
    public TaskDTO changeTaskStatusById(@Nullable final String userId,
                                        @Nullable final String id,
                                        @Nullable final Status status) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null || status.getDisplayName().isEmpty()) throw new StatusEmptyException();
        @Nullable final TaskDTO task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return update(task);
    }

    @Override
    public void clear() throws Exception {
        repository.deleteAll();
    }

    @Override
    public void clear(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final List<TaskDTO> taskDTOList = findAll(userId);
        if (taskDTOList == null || taskDTOList.isEmpty()) throw new TaskNotFoundException();
        repository.deleteAll(taskDTOList);
    }

}
