package ru.t1.ytarasov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.ytarasov.tm.api.repository.model.ISessionRepository;
import ru.t1.ytarasov.tm.api.repository.model.IUserRepository;
import ru.t1.ytarasov.tm.api.service.model.ISessionService;
import ru.t1.ytarasov.tm.exception.entity.SessionNotFoundException;
import ru.t1.ytarasov.tm.exception.field.IdEmptyException;
import ru.t1.ytarasov.tm.exception.field.UserIdEmptyException;
import ru.t1.ytarasov.tm.exception.user.UserNotFoundException;
import ru.t1.ytarasov.tm.model.Session;
import ru.t1.ytarasov.tm.model.User;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Service
public final class SessionService
        extends AbstractUserOwnedService<Session> implements ISessionService {

    @NotNull
    @Autowired
    private ISessionRepository repository;

    @NotNull
    @Autowired
    private IUserRepository userRepository;

    @Nullable
    @Override
    public List<Session> findAll() throws Exception {
        return repository.findAll();
    }

    @Override
    public Long getSize() throws Exception {
        return repository.count();
    }

    @NotNull
    @Override
    public Session add(@Nullable Session model) throws Exception {
        if (model == null) throw new SessionNotFoundException();
        repository.saveAndFlush(model);
        return model;
    }

    @Nullable
    @Override
    public Collection<Session> add(@Nullable Collection<Session> models) throws Exception {
        if (models == null || models.isEmpty()) throw new SessionNotFoundException();
        repository.saveAll(models);
        return models;
    }

    @Override
    public boolean existsById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsById(id);
    }

    @Nullable
    @Override
    public Session findOneById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findById(id).orElse(null);
    }

    @Override
    public void clear() throws Exception {
        repository.deleteAll();
    }

    @Nullable
    @Override
    public Session remove(@Nullable Session model) throws Exception {
        if (model == null) throw new SessionNotFoundException();
        repository.delete(model);
        return model;
    }

    @Nullable
    @Override
    public Session removeById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Session session = findOneById(id);
        if (session == null) throw new SessionNotFoundException();
        repository.deleteById(id);
        return session;
    }

    @NotNull
    @Override
    public Session update(@Nullable Session model) throws Exception {
        if (model == null) throw new SessionNotFoundException();
        model.setUpdated(new Date());
        repository.saveAndFlush(model);
        return model;
    }

    @Override
    public void clear(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final List<Session> sessions = findAll(userId);
        if (sessions == null || sessions.isEmpty()) throw new SessionNotFoundException();
        repository.deleteAll(sessions);
    }

    @Override
    public boolean existsById(@Nullable String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.existsByIdWithUserId(userId, id);
    }

    @Nullable
    @Override
    public  List<Session> findAll(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAllWithUserId(userId);
    }

    @Nullable
    @Override
    public Session findOneById(@Nullable String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(userId, id);
    }

    @Override
    public Long getSize(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.getSize(userId);
    }

    @Nullable
    @Override
    public Session removeById(@Nullable String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Session session = findOneById(userId, id);
        if (session == null) throw new SessionNotFoundException();
        repository.delete(session);
        return session;
    }

    @NotNull
    @Override
    public Session add(@Nullable String userId, @Nullable Session model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new SessionNotFoundException();
        @Nullable final User user = userRepository.findById(userId).orElse(null);
        if (user == null) throw new UserNotFoundException();
        model.setUser(user);
        return add(model);
    }

    @Nullable
    @Override
    public Session remove(@Nullable String userId, @Nullable Session model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new SessionNotFoundException();
        @Nullable final Session session = findOneById(userId, model.getId());
        if (session == null) throw new SessionNotFoundException();
        return remove(session);
    }

}
