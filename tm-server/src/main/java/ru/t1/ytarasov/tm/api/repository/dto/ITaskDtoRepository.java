package ru.t1.ytarasov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ytarasov.tm.dto.model.TaskDTO;

import java.util.List;

@Repository
public interface ITaskDtoRepository extends IUserOwnedDtoRepository<TaskDTO> {

    @Nullable
    @Query("SELECT t FROM TaskDTO t ORDER BY :sortType")
    List<TaskDTO> findAllWithSort(@NotNull @Param("sortType") final String sortType);

    @Nullable
    @Query("SELECT t FROM TaskDTO t WHERE t.userId = :userId ORDER BY :sortType")
    List<TaskDTO> findAllWithUserIdAndSort(
            @NotNull @Param("userId") final String userId,
            @NotNull @Param("sortType") final String sortType
    );

    @Nullable
    @Query("SELECT t FROM TaskDTO t WHERE t.userId = :userId")
    List<TaskDTO> findAllByUserId(@NotNull @Param("userId") final String userId);

    @Nullable
    @Query("SELECT t FROM TaskDTO t WHERE t.id = :id AND t.userId = :userId")
    TaskDTO findOneById(@NotNull @Param("userId") final String userId, @NotNull @Param("id") final String id);

    @Query("SELECT COUNT(t) FROM TaskDTO t WHERE t.userId = :userId")
    Long getSize(@NotNull @Param("userId") final String id);

    @Query("SELECT COUNT(1) = 1 FROM TaskDTO t WHERE t.id = :id AND t.userId = :userId")
    Boolean existsByIdAndUserId(@NotNull @Param("userId") final String userId, @NotNull @Param("id") final String id);

    @NotNull
    @Query("SELECT t FROM TaskDTO t WHERE t.userId = :userId AND t.projectId = :projectId")
    List<TaskDTO> findAllTasksByProjectId(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("projectId") String projectId
    );

}
