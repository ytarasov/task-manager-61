package ru.t1.ytarasov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.ytarasov.tm.api.repository.model.IUserRepository;
import ru.t1.ytarasov.tm.api.service.IPropertyService;
import ru.t1.ytarasov.tm.api.service.model.IUserService;
import ru.t1.ytarasov.tm.enumerated.Role;
import ru.t1.ytarasov.tm.exception.field.*;
import ru.t1.ytarasov.tm.exception.user.ExistsEmailException;
import ru.t1.ytarasov.tm.exception.user.ExistsLoginException;
import ru.t1.ytarasov.tm.exception.user.UserNotFoundException;
import ru.t1.ytarasov.tm.model.User;
import ru.t1.ytarasov.tm.util.HashUtil;

import java.util.Collection;
import java.util.Date;
import java.util.List;

@Service
public final class UserService
        extends AbstractService<User> implements IUserService {

    @NotNull
    @Autowired
    private IUserRepository repository;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Nullable
    @Override
    public List<User> findAll() throws Exception {
        return repository.findAll();
    }

    @Override
    public Long getSize() throws Exception {
        return repository.count();
    }

    @NotNull
    @Override
    public User add(@Nullable final User model) throws Exception {
        if (model == null) throw new UserNotFoundException();
        repository.saveAndFlush(model);
        return model;
    }

    @Nullable
    @Override
    public Collection<User> add(@Nullable Collection<User> models) throws Exception {
        if (models == null || models.isEmpty()) throw new UserNotFoundException();
        repository.saveAll(models);
        return models;
    }

    @Override
    public boolean existsById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsById(id);
    }

    @Nullable
    @Override
    public User findOneById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findById(id).orElse(null);
    }

    @Override
    public void clear() throws Exception {
        repository.deleteAll();
    }

    @Nullable
    @Override
    public User remove(@Nullable User model) throws Exception {
        if (model == null) throw new UserNotFoundException();
        repository.delete(model);
        return model;
    }

    @Nullable
    @Override
    public User removeById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final User user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        repository.deleteById(id);
        return user;
    }

    @NotNull
    @Override
    public User update(@Nullable final User model) throws Exception {
        if (model == null) throw new UserNotFoundException();
        model.setUpdated(new Date());
        repository.saveAndFlush(model);
        return model;
    }

    @Override
    public @NotNull User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email,
            @Nullable final Role role
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isEmailExists(email)) throw new ExistsEmailException();
        if (role == null) throw new RoleEmptyException();
        @Nullable final String passwordHash = HashUtil.salt(propertyService, password);
        if (passwordHash == null) throw new PasswordEmptyException();
        @NotNull final User user = new User(login, passwordHash, email);
        user.setRole(role);
        return add(user);
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return repository.findByLogin(login);
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return repository.findByEmail(email);
    }

    @Nullable
    @Override
    public User removeByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        return remove(user);
    }

    @Override
    public void lockUser(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        update(user);
    }

    @Override
    public void unlockUser(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        update(user);
    }

    @Override
    public void changePassword(@Nullable final String id, @Nullable final String newPassword) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (newPassword == null || newPassword.isEmpty()) throw new PasswordEmptyException();
        @Nullable final String newPasswordHash = HashUtil.salt(propertyService, newPassword);
        @Nullable final User user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(newPasswordHash);
        update(user);
    }

    @Override
    public void updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final User user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        user.setUpdated(new Date());
        update(user);
    }

    @Override
    public boolean isLoginExist(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return repository.isLoginExists(login);
    }

    @Override
    public boolean isEmailExists(@Nullable String email) throws Exception {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return repository.isEmailExists(email);
    }

}
