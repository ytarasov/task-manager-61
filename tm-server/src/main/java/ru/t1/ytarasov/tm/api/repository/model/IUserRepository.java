package ru.t1.ytarasov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.t1.ytarasov.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    @Query("SELECT u FROM User u WHERE u.login = :login")
    User findByLogin(@NotNull @Param("login") final String login);

    @Nullable
    @Query("SELECT u FROM User u WHERE u.email = :email")
    User findByEmail(@NotNull @Param("email") final String email);

    @Query("SELECT COUNT(1) = 1 FROM User u WHERE u.login = :login")
    Boolean isLoginExists(@NotNull @Param("login") final String login);

    @Query("SELECT COUNT(1) = 1 FROM User u WHERE u.email = :email")
    Boolean isEmailExists(@NotNull @Param("email") final String email);

}
