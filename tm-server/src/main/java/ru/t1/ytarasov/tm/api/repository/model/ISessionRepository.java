package ru.t1.ytarasov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.ytarasov.tm.model.Session;

import java.util.List;

@Repository
public interface ISessionRepository extends IUserOwnedRepository<Session> {

    @Nullable
    @Query("SELECT s FROM Session s WHERE s.user.id = :userId")
    List<Session> findAllWithUserId(@NotNull @Param("userId") final String userId);

    @Nullable
    @Query("SELECT s FROM Session s WHERE s.id = :id AND s.user.id = :userId")
    Session findOneById(@NotNull @Param("id") final String userId, @NotNull @Param("userId") final String id);

    @Query("SELECT COUNT(s) FROM Session s WHERE s.user.id = :userId")
    Long getSize(@NotNull @Param("userId") final String userId);

    @Query("SELECT COUNT(1) = 1 FROM Session s WHERE s.id = :id AND s.user.id = :userId")
    Boolean existsByIdWithUserId(@NotNull @Param("userId") final String userId, @NotNull @Param("id") final String id);

}
