package ru.t1.ytarasov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.ytarasov.tm.model.Project;

import java.util.List;

@Repository
public interface IProjectRepository extends IUserOwnedRepository<Project> {

    @Nullable
    @Query("SELECT p FROM Project p ORDER BY :sortOrder")
    List<Project> findAllWithSort(@NotNull @Param("sortOrder") final String sortOrder);

    @Nullable
    @Query("SELECT p FROM Project p WHERE p.user.id = :userId ORDER BY :sortOrder")
    List<Project> findAllWithUserIdAndSort(
            @NotNull @Param("userId") final String userId,
            @NotNull @Param("sortOrder") final String sortOrder
    );

    @Nullable
    @Query("SELECT p FROM Project p WHERE p.user.id = :userId")
    List<Project> findAllWithUserId(@NotNull @Param("userId") final String userId);

    @Nullable
    @Query("SELECT p FROM Project p WHERE p.user.id = :userId AND p.id = :id")
    Project findOneById(@NotNull @Param("userId") final String userId, @NotNull @Param("id") final String id);

    @Query("SELECT COUNT(p) FROM Project p WHERE p.user.id = :userId")
    Long getSize(@NotNull @Param("userId") final String userId);

    @Query("SELECT COUNT(1) = 1 FROM Project p WHERE p.id = :id AND p.user.id = :userId")
    Boolean existsByIdWithUserId(@NotNull @Param("userId") final String userId, @NotNull @Param("id") final String id);

}
