package ru.t1.ytarasov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ytarasov.tm.dto.model.SessionDTO;

import java.util.List;

@Repository
public interface ISessionDtoRepository extends IUserOwnedDtoRepository<SessionDTO> {

    @Nullable
    @Query("SELECT s FROM SessionDTO s WHERE s.userId = :userId ORDER BY :sortType")
    List<SessionDTO> findAllWithSort(@NotNull @Param("sortType") final String sortType);

    @Nullable
    @Query("SELECT s FROM SessionDTO s WHERE s.userId = :userId ORDER BY :sortType")
    List<SessionDTO> findAllWithUserIdAndSort(
            @NotNull @Param("userId") final String userId,
            @NotNull @Param("sortType") final String sortType
    );

    @Nullable
    @Query("SELECT s FROM SessionDTO s WHERE s.userId = :userId")
    List<SessionDTO> findAllByUserId(@NotNull @Param("userId") final String userId);

    @Nullable
    @Query("SELECT s FROM SessionDTO s WHERE s.id = :id AND s.userId=:userId")
    SessionDTO findOneById(@NotNull @Param("userId") final String userId, @NotNull @Param("id") final String id);

    @Query("SELECT COUNT(s) FROM SessionDTO s WHERE s.userId = :userId")
    Long getSize(@NotNull @Param("userId") final String id);

    @Query("SELECT COUNT(1) = 1 FROM SessionDTO s WHERE s.id = :id AND s.userId = :userId")
    Boolean existsByIdAndUserId(@NotNull @Param("userId") final String userId, @NotNull @Param("id") final String id);

}
