package ru.t1.ytarasov.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.ytarasov.tm.api.service.IPropertyService;

import java.util.Properties;

@Getter
@Service
@PropertySource("classpath:application.properties")
public class PropertyService implements IPropertyService {

    @NotNull
    public static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    private static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    @Value("#{environment['server.port']}")
    private String port;

    @NotNull
    @Value("#{environment['server.host']}")
    private String host;

    @NotNull
    @Value("#{environment['password.secret']}")
    private String passwordSecret;

    @NotNull
    @Value("#{environment['password.iteration']}")
    private Integer passwordIteration;

    @NotNull
    @Value("#{environment['session.key']}")
    private String sessionKey;

    @NotNull
    @Value("#{environment['session.timeout']}")
    private Integer sessionTimeout;

    @NotNull
    @Value("#{environment['database.url']}")
    private String dbUrl;

    @NotNull
    @Value("#{environment['database.username']}")
    private String dbUsername;

    @NotNull
    @Value("#{environment['database.password']}")
    private String dbPassword;

    @NotNull
    @Value("#{environment['database.driver']}")
    private String dbDriver;

    @NotNull
    @Value("#{environment['database.dialect']}")
    private String dbDialect;

    @NotNull
    @Value("#{environment['database.hbm2ddl_auto']}")
    private String dbHbm2ddlAuto;

    @NotNull
    @Value("#{environment['database.show_sql']}")
    private String showSql;

    @NotNull
    @Value("#{environment['database.format_sql']}")
    private String formatSql;

    @NotNull
    @Value("#{environment['database.second_lvl_cache']}")
    private String secondLvlCash;

    @NotNull
    @Value("#{environment['database.factory_class']}")
    private String factoryClass;

    @NotNull
    @Value("#{environment['database.use_query_cache']}")
    private String useQueryCash;

    @NotNull
    @Value("#{environment['database.use_min_puts']}")
    private String useMinPuts;

    @NotNull
    @Value("#{environment['database.region_prefix']}")
    private String regionPrefix;

    @NotNull
    @Value("#{environment['database.config_file_path']}")
    private String configFilePath;

    @Override
    @NotNull
    public String getAuthorName() {
        return Manifests.read(AUTHOR_NAME_KEY);
    }

    @Override
    @NotNull
    public String getAuthorEmail() {
        return Manifests.read(AUTHOR_EMAIL_KEY);
    }

    @Override
    @NotNull
    public String getApplicationVersion() {
        return Manifests.read(APPLICATION_VERSION_KEY);
    }

}
