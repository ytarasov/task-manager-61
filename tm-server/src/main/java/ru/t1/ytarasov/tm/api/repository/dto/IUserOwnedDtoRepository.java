package ru.t1.ytarasov.tm.api.repository.dto;

import org.springframework.stereotype.Repository;
import ru.t1.ytarasov.tm.dto.model.AbstractUserOwnedModelDTO;

@Repository
public interface IUserOwnedDtoRepository<M extends AbstractUserOwnedModelDTO> extends IDtoRepository<M> {
}
