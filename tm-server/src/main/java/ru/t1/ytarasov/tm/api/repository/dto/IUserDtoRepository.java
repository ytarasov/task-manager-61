package ru.t1.ytarasov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ytarasov.tm.dto.model.UserDTO;

@Repository
public interface IUserDtoRepository extends IDtoRepository<UserDTO> {

    @Nullable
    @Query("SELECT u FROM UserDTO u WHERE u.login = :login")
    UserDTO findByLogin(@NotNull @Param("login") final String login);

    @Nullable
    @Query("SELECT u FROM UserDTO u WHERE u.email = :email")
    UserDTO findByEmail(@NotNull @Param("email") final String email);

    @Query("SELECT COUNT(1) = 1 FROM UserDTO u WHERE u.login = :login")
    Boolean isLoginExists(@NotNull @Param("login") final String login);

    @Query("SELECT COUNT(1) = 1 FROM UserDTO u WHERE u.email = :email")
    Boolean isEmailExists(@NotNull @Param("email")  final String email);

}
