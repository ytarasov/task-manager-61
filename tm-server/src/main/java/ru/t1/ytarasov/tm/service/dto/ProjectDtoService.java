package ru.t1.ytarasov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.ytarasov.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.ytarasov.tm.api.service.dto.IProjectDtoService;
import ru.t1.ytarasov.tm.enumerated.Sort;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.ytarasov.tm.exception.field.*;
import ru.t1.ytarasov.tm.dto.model.ProjectDTO;

import java.util.*;

@Service
public final class ProjectDtoService
        extends AbstractUserOwnedDtoService<ProjectDTO> implements IProjectDtoService {

    @NotNull
    @Autowired
    private IProjectDtoRepository repository;

    @Override
    @NotNull
    public ProjectDTO add(@Nullable final ProjectDTO project) throws Exception {
        if (project == null) throw new ProjectNotFoundException();
        repository.saveAndFlush(project);
        return project;
    }

    @NotNull
    @Override
    public Collection<ProjectDTO> add(@Nullable Collection<ProjectDTO> models) throws Exception {
        if (models == null || models.isEmpty()) throw new ProjectNotFoundException();
        repository.saveAll(models);
        return models;
    }

    @NotNull
    @Override
    public ProjectDTO updateById(@Nullable final String userId,
                                 @Nullable final String id,
                                 @Nullable final String name,
                                 @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final ProjectDTO project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return update(project);
    }

    @Override
    public void clear() throws Exception {
        repository.deleteAll();
    }

    @Override
    public void clear(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final List<ProjectDTO> projectDTOList = findAll(userId);
        if (projectDTOList == null || projectDTOList.isEmpty()) throw new ProjectNotFoundException();
        repository.deleteAll(projectDTOList);
    }

    @NotNull
    @Override
    public ProjectDTO create(@Nullable String userId, @Nullable String name, @Nullable String description) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final ProjectDTO project = new ProjectDTO(name, description);
        project.setUserId(userId);
        return add(project);
    }

    @NotNull
    @Override
    public ProjectDTO changeProjectStatusById(@Nullable final String userId,
                                              @Nullable final String id,
                                              @Nullable final Status status) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null || status.getDisplayName().isEmpty()) throw new StatusEmptyException();
        @Nullable final ProjectDTO project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return update(project);
    }

    @Override
    public @NotNull ProjectDTO update(@Nullable ProjectDTO project) throws Exception {
        if (project == null) throw new ProjectNotFoundException();
        project.setUpdated(new Date());
        repository.saveAndFlush(project);
        return project;
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAll() {
        return repository.findAll();
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAll(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAllByUserId(userId);
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAll(@Nullable Sort sort) throws Exception {
        if (sort == null) return findAll();
        else return repository.findAllWithSort(getSortType(sort));
    }

    @Override
    public Long getSize() throws Exception {
        return repository.count();
    }

    @Override
    public Long getSize(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.getSize(userId);
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAll(@Nullable String userId, @Nullable Sort sort) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        else return repository.findAllWithUserIdAndSort(userId, getSortType(sort));
    }

    @Nullable
    @Override
    public ProjectDTO findOneById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findById(id).orElse(null);
    }

    @Nullable
    @Override
    public ProjectDTO findOneById(@Nullable String userId, @Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findOneById(userId, id);
    }

    @Nullable
    @Override
    public ProjectDTO remove(@Nullable final ProjectDTO project) throws Exception {
        if (project == null) throw new ProjectNotFoundException();
        repository.delete(project);
        return project;
    }

    @Nullable
    @Override
    public void removeById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final ProjectDTO project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        repository.deleteById(id);
    }

    @Nullable
    @Override
    public ProjectDTO removeById(@Nullable String userId, @Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final ProjectDTO project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        return remove(project);
    }

    @Nullable
    @Override
    public ProjectDTO add(@Nullable String userId, @Nullable ProjectDTO model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ProjectNotFoundException();
        model.setUserId(userId);
        return add(model);
    }

    @Nullable
    @Override
    public ProjectDTO remove(@Nullable String userId, @Nullable ProjectDTO model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ProjectNotFoundException();
        @Nullable final ProjectDTO projectDTO = findOneById(userId, model.getId());
        if (projectDTO == null) throw new ProjectNotFoundException();
        return remove(projectDTO);
    }

    @Override
    public Boolean existsById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsById(id);
    }

    @Override
    public Boolean existsById(@Nullable String userId, @Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.existsByIdAndUserId(userId, id);
    }

}
